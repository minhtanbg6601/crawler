import { Injectable } from '@nestjs/common';
import { Command, Console } from 'nestjs-console';
import { AkaBookCrawler } from './waka';



@Console()
@Injectable()
export class CrawlerConsole {


    constructor() { }

    // job provider here

    // job consumer
    @Command({
        command: 'crawler-waka-consumer',
        description: 'manual add job to queue',
    })
    async startCrawler() {
        const crawler = new AkaBookCrawler(
            'https://ebook.waka.vn/muon-kiep-nhan-sinh-nguyen-phong-rqa6xW.html?type=1',
            {
                "d_v_i2": "qnjnb37da6futsv6pdh9r92ih3",
                "hide_group": "0",
                "lhc_per": "{\"vid\":\"v6kuwz016z4r5tlg67cj\"}",
                "auth._token_expiration.local": "false",
                "auth._token.local": "false",
                "auth.device_id": "bc068425228c9eda83b289bbc2ef99c4",
                "auth.strategy": "local",
                "auth.fm.auth.accessToken": "1a37828677c10018e1706524647p0c1256800269.5fda51121d845971340ec15ab78efcfa",
                "auth.fm.auth.refreshToken": "2a37828677c10018e1990348647p0c587248550.4b25d478cb5d10c92d0ec288b2582668"
            })
        await crawler.start()
    }
}
