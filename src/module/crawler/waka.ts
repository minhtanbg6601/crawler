import { writeFileSync } from "fs";
import puppeteer, { Browser, Page } from "puppeteer"
type Cookies = {
    name: string;
    value: string
}
export class AkaBookCrawler {
    private pageData: string[]
    private page: Page;
    private browser: Browser;
    private cookies: Array<Cookies>;
    url: string;
    private iframeSelector = 'iframe#__r__'
    constructor(url: string, cookies: Object) {
        this.cookies = Object.keys(cookies).map(c => ({ 'name': c, value: cookies[c] }));
        this.url = url;
        this.pageData=[]
    }
    private async _init() {
        this.browser = await puppeteer.launch()
        this.page = await this.browser.newPage();
        await this.page.goto(this.url)
        await this.page.setViewport({ width: 1080, height: 10000000 });
        await this.page.setCookie(...this.cookies)
    }
    public async start() {
        await this._init()
        await this.crawlPage()
    }
    private async crawlPage() {
        // crawl first page, crawl pages between, crawl end page.


        let counter=0;
        while (true) {
            counter++;
            if(counter===4) break;
            
            if (this.pageData.length === 0) {
                // reset the book to first page.
            }
            console.log('====================================');
            console.log('begin crawling');
            console.log('====================================');
            // wait for iframe to load.

            await this.page.waitForSelector(this.iframeSelector);
            await this.page.waitForFunction((selector, color) => {
                const element = document.querySelector(selector);
                const style = window.getComputedStyle(element);
                return style.visibility === 'visible';
            }, {}, this.iframeSelector, 'red');
            console.log('====================================');
            console.log('iframe loaded');
            console.log('====================================');

            const iframeElement = await this.page.$(this.iframeSelector);
            const iframeRef = await iframeElement.contentFrame()
            await iframeRef.waitForSelector('body') // wait for body to load.
            console.log('====================================');
            console.log('body loaded');
            console.log('====================================');
            const body = await iframeRef.$('body')
            // first page
            const data = await body.evaluate(e => e.innerHTML)
            this.pageData.push(data)


            const isNextPageDisable=await this.page.$('div#nextPage.disabled');
            if(isNextPageDisable){
                break;
            }
            // next page navigation, check end of book.
            await this.page.click('div#nextPage')

            // TODO: determine next page is loaded more precisely.
            await new Promise((res,ej)=>{
                setTimeout(()=>{
                    res('ok')
                },10000)
            })
            
            console.log('====================================');
            console.log(this.pageData[this.pageData.length-1].substring(0,150));
            console.log('====================================');
        }
        console.log('====================================');
        console.log('crawl complete on ', this.url);
        console.log('====================================');
        writeFileSync('output.html',this.pageData.join('\n'))
    }
}

