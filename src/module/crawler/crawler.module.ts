import { Module } from "@nestjs/common";

import {CrawlerConsole} from './crawler.console'

@Module({
imports:[],
providers:[CrawlerConsole]
})
export class CrawlerModule{}