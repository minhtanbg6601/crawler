import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConsoleModule } from 'nestjs-console';
import { CrawlerModule } from './module/crawler/crawler.module';

@Module({
  imports: [ConsoleModule,CrawlerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
